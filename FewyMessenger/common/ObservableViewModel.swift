//
//  ObservableViewModel.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 31.10.21.
//

import Foundation

class ObservableViewModel {
    
    var observerController : ObserverController?
    
    init(){}
    
    func subscribe(observer: ObserverController){
        self.observerController = observer
    }
    
    func unSubscribe(observer: ObserverController){
        self.observerController = nil
    }
    
    func notifyChanges(){
        if(observerController != nil){
            observerController!.update()
        }
    }
}

protocol ObserverController {
    
    func update()
}


