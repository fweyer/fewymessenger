//
//  ChatControllerCollectionViewController.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 01.11.21.
//

import UIKit


class ChatController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var viewModel: ChatViewModel!
    
    @IBOutlet weak var chatMessagesTable: UITableView!
    @IBOutlet weak var uiTextfield: UITextField!
    @IBOutlet weak var uiButtonSenden: UIButton!
    
    var uiTextfieldContraintBottom : NSLayoutConstraint?
    var uiButtonContraintBottom : NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ChatViewModel.getInstance()
        viewModel.subscribe(observer: self)
        viewModel.reloadMessageList()
        chatMessagesTable.delegate = self
        chatMessagesTable.dataSource = self
        chatMessagesTable.alwaysBounceVertical = true
        setupTopBar()
        setupKeyBoardShowing()
        chatMessagesTable.backgroundColor = UIColor.white
        DispatchQueue.main.async{
            self.scrollToBottom(animation: false)
        }
    }
    
    @IBAction func onSendMessageClick(_ sender: Any) {
        SendMessage(content: uiTextfield.text).execute()
        uiTextfield.text = ""
        self.view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseCellUser", for: indexPath) as! ChatBubbleCell
        let message = viewModel.getMessage(index: indexPath.row)
        formatCells(cell: cell, message: message)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = viewModel.getMessage(index: indexPath.row)
        let size = ChatBubbleCell.setupFrameForText(text: message.content)
        return CGFloat(size.height + 30)
    }
    
    
    func formatCells(cell: ChatBubbleCell, message: Message){
        cell.textView.text = message.content
        cell.bubbleViewWidth?.constant = ChatBubbleCell.setupFrameForText(text: message.content).width + 32
        if viewModel.currentProfilUser?.uid == message.fromUserID {
            cell.bubbleView.backgroundColor = ChatBubbleCell.greenColor
            cell.bubbleViewShowRight?.isActive = true
            cell.bubbleViewShowLeft?.isActive = false
        }else{
            cell.bubbleView.backgroundColor = UIColor.systemBlue
            cell.bubbleViewShowRight?.isActive = false
            cell.bubbleViewShowLeft?.isActive = true
        }
    }
    
    private func scrollToBottom(animation: Bool){
        let numberOfSections = chatMessagesTable.numberOfSections
        let numberOfRows = chatMessagesTable.numberOfRows(inSection: numberOfSections-1)
        let indexPath = IndexPath(row: numberOfRows-1 , section: numberOfSections-1)
        if numberOfRows > 0 {
            chatMessagesTable.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: animation)
        }
    
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension ChatController : ObserverController{
    func update() {
        chatMessagesTable.reloadData()
        scrollToBottom(animation: true)
    }
}

extension ChatController {
    
    func setupTopBar(){
        setupBackButton()
        setNavigation()
        setupInputtextField()
        let user = viewModel.userToChat
        if (user == nil) { return }
        setupNavigtionBarWihtUser(user: user!)
    }
    
    //Navigationbar ändert beim scrollen die Farbe deshalb dieser codefix
    func setNavigation() {
        self.navigationController?.navigationBar.tintColor = .red
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    
    func setupBackButton(){
        self.navigationItem.hidesBackButton = true
        let backButton = UIBarButtonItem(title: "zurück", style: .plain, target: self, action: #selector(back))
        backButton.tintColor = UIColor.systemBlue
        self.navigationItem.leftBarButtonItem = backButton
    }
    
    func setupInputtextField(){
        uiTextfield.backgroundColor = UIColor.white
        uiTextfield.textColor = UIColor.black
        uiTextfield.layer.cornerRadius = 10
        uiTextfield.layer.borderColor = UIColor.lightGray.cgColor
        uiTextfield.layer.borderWidth = 1.0
        uiTextfield.attributedPlaceholder = NSAttributedString(
            string: "Nachricht senden ...",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
    }
    
    func setupNavigtionBarWihtUser(user: User){
        let cqRect = CGRect(x: 0, y: 0, width: 100, height: 40)
        let titleView = NavigationBarTitleWithUserView(cgRect: cqRect, user: user)
        navigationItem.titleView = titleView
    }
    
    @objc func back(sender: UIBarButtonItem){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func setupKeyBoardShowing(){
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardWillShow),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardWillHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
        uiTextfieldContraintBottom = uiTextfield.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        uiButtonContraintBottom = uiButtonSenden.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        
    }
    
    @objc func handleKeyBoardWillShow(notification : NSNotification){
        let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        uiTextfieldContraintBottom?.constant = -keyboardFrame!.height
        uiTextfieldContraintBottom?.isActive = true
        uiButtonContraintBottom?.constant = -keyboardFrame!.height
        uiButtonContraintBottom?.isActive = true
        DispatchQueue.main.async{
            self.scrollToBottom(animation: false)
        }
    }
    
    @objc func handleKeyBoardWillHide(){
        uiTextfieldContraintBottom?.constant = 5
        uiTextfieldContraintBottom?.isActive = true
        uiButtonContraintBottom?.constant = 5
        uiButtonContraintBottom?.isActive = true
    }
    
}

    

