//
//  RegisterViewController.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 07.10.21.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var uiTextfieldName: UITextField!
    @IBOutlet weak var uiTextfieldEmail: UITextField!
    @IBOutlet weak var uiTextfieldPasswort: UITextField!
    @IBOutlet weak var uiImageView: UIImageView!
    
    static var currentController: RegisterViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RegisterViewController.currentController = self
        
        //Extensions
        setupLabelButtons()
        setupRoundedImageView()
    }
    
    @IBAction func onRegistrierenClick(_ sender: Any) {
        RegisterUser(
            name: uiTextfieldName.text,
            email: uiTextfieldEmail.text,
            password: uiTextfieldPasswort.text
        ).execute()
    }
    
    @IBAction func onZumLoginClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showDialogInputDataUnCompled() {
        let dialog = DialogFactory.instance.createStandardDialog(title: "Fehler Login", message: "Eingabedaten sind unvollständig!")
        self.present(dialog, animated: true, completion: nil)
    }
    
    func showDialogRegisterSucces(){
        self.dismiss(animated: false){
            let dialog = DialogFactory.instance.createStandardDialog(title: "Registrierung", message: "Erfolgreich registriert")
            self.present(dialog, animated: true, completion: nil)
        }
    }
    
    func showLoadingDialog(){
        self.present(DialogFactory.instance.createLoadingAnimationDialog(), animated: true, completion: nil)
    }
    
    func showDialogOnRegisterError(){
        self.dismiss(animated: false) {
            let dialog = DialogFactory.instance.createStandardDialog(title: "Fehler", message: "Es ist ein Fehler aufgetreten!")
            self.present(dialog, animated: true, completion: nil)
        }
    }
    
}

// Extenion für das ImageView
extension RegisterViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    func setupRoundedImageView(){
        uiImageView.layer.masksToBounds = false
        uiImageView.layer.borderWidth = 0
        uiImageView.layer.cornerRadius = uiImageView.frame.size.height / 2
        uiImageView.clipsToBounds = true
        addClickGestureImageView()
    }
    
    func addClickGestureImageView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleSelectImage))
        uiImageView.addGestureRecognizer(tapGesture)
        uiImageView.isUserInteractionEnabled = true
    }
    
    @objc func handleSelectImage(){
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        present(pickerController, animated: true, completion: nil)
    }
    
    // Sorgt dafür das, das gewähle Image auch im Container übernommen wird. Methode kommt aus dem Protocol UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editImage = info[.editedImage] as? UIImage {
            uiImageView.image = editImage
        }
        
        if let originalImage = info[.originalImage] as? UIImage {
            uiImageView.image = originalImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension RegisterViewController {
    func setupLabelButtons (){
        uiTextfieldName.backgroundColor = UIColor.white
        uiTextfieldName.textColor = UIColor.black
        uiTextfieldName.attributedPlaceholder = NSAttributedString(
            string: "Name",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
        uiTextfieldEmail.backgroundColor = UIColor.white
        uiTextfieldEmail.textColor = UIColor.black
        uiTextfieldEmail.attributedPlaceholder = NSAttributedString(
            string: "E-Mail",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
        uiTextfieldPasswort.backgroundColor = UIColor.white
        uiTextfieldPasswort.textColor = UIColor.black
        uiTextfieldPasswort.attributedPlaceholder = NSAttributedString(
            string: "Passwort",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
    }
}
