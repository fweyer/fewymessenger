//
//  ViewController.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 06.10.21.
//

import UIKit

class LoginsViewController: UIViewController {

    
    @IBOutlet weak var uiTextfieldEmail: UITextField!    
    @IBOutlet weak var uiTextfieldPasswort: UITextField!
    
    static var currentController: LoginsViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLabelButtons()
        LoginsViewController.currentController = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        AutoLogin().execute()
    }
    
    @IBAction func onLoginClick(_ sender: Any) {
        LoginUseCase(email: uiTextfieldEmail.text, password: uiTextfieldPasswort.text).execute()
    }
    
    @IBAction func onRegisterClick(_ sender: Any) {
        self.performSegue(withIdentifier: "toRegisterViewController", sender: nil)
    }
    
    func showLoadingDialog(){
        self.present(DialogFactory.instance.createLoadingAnimationDialog(), animated: true, completion: nil)
    }
    
    func navigateToMessageTableView(){
        self.dismiss(animated: false){
            self.performSegue(withIdentifier: "toMessageTableViewController", sender: nil)
        }
    }
    
    func showErrorDialogPasswordOrEmailWrong(){
        self.dismiss(animated: false) {
            let dialog = DialogFactory.instance.createStandardDialog(title: "Fehler Login", message: "Passwort oder Email ist falsch")
            self.present(dialog, animated: true, completion: nil)
        }
    }
}

extension LoginsViewController {
    func setupLabelButtons (){
        uiTextfieldEmail.backgroundColor = UIColor.white
        uiTextfieldEmail.textColor = UIColor.black
        uiTextfieldEmail.attributedPlaceholder = NSAttributedString(
            string: "E-Mail",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
        uiTextfieldPasswort.backgroundColor = UIColor.white
        uiTextfieldPasswort.textColor = UIColor.black
        uiTextfieldPasswort.attributedPlaceholder = NSAttributedString(
            string: "Passwort",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
    }
}
