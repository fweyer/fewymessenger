//
//  NewFriendlistControllerTableViewController.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 30.10.21.
//

import UIKit

class KontaktListTableViewController: UITableViewController {
    
    @IBOutlet weak var userTableView: UITableView!
    
    var viewModel: KontaktListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = KontaktListViewModel.getInstance()
        viewModel.subscribe(observer: self)
        self.tableView.register(KontaktCellTableViewCell.self, forCellReuseIdentifier: "reuseCellUser")
        userTableView.delegate = self
        userTableView.dataSource = self
        setupTopBar()
        userTableView.backgroundColor = UIColor.white
        ShowKontaktList().execute()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.kontaktList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseCellUser", for: indexPath) as! KontaktCellTableViewCell
        cell.nameLabel?.text = viewModel.kontaktList[indexPath.row].name
        cell.emailLabel?.text = viewModel.kontaktList[indexPath.row].email
        setProfilImage(cell: cell, url: viewModel.kontaktList[indexPath.row].profileImageURL)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedUser = viewModel.kontaktList[indexPath.row]
        ChatToUser(userToChat:  selectedUser).execute()
        self.performSegue(withIdentifier: "fromNewFriendsToChat", sender: nil)
    }
    
    @IBAction func onLogoutButtonClick(_ sender: Any) {
        UserRepository.getInstance().userLogout()
        self.dismiss(animated: true, completion: nil)
    }
    
    func showLoadingDialog(){
        self.present(DialogFactory.instance.createLoadingAnimationDialog(), animated: true, completion: nil)
    }
    
    private func setProfilImage(cell: KontaktCellTableViewCell, url: String?){
        if cell.profilImageView == nil {
            return
        }
        if let linkToImage = url {
            let profilUrl = URL(string: linkToImage)
            cell.profilImageView.sd_setImage(with: profilUrl)
        }
    }
}

extension KontaktListTableViewController {
    func setupTopBar(){
        setupBackButton()
        if(viewModel.currentProfilUser != nil) {
            setupNavigtionBarWihtUser(user: viewModel.currentProfilUser!)
        }
    }
    
    func setupBackButton(){
        let backButton = UIBarButtonItem()
        backButton.title = "zurück"
        backButton.tintColor = UIColor.systemBlue
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    func setupNavigtionBarWihtUser(user: User){
        let cqRect = CGRect(x: 0, y: 0, width: 100, height: 40)
        let titleView = NavigationBarTitleWithUserView(cgRect: cqRect, user: user)
        navigationItem.titleView = titleView
    }
    
}

extension KontaktListTableViewController : ObserverController{
    func update() {
        self.userTableView.reloadData()
        if(viewModel.currentProfilUser != nil) {
            setupNavigtionBarWihtUser(user: viewModel.currentProfilUser!)
        }
    }
}

