//
//  MessageTableViewController.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 16.10.21.
//

import UIKit
import SDWebImage

class MessageTableViewController: UITableViewController {

    @IBOutlet weak var userTableView: UITableView!
    
    var viewModel: MessageTableViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MessageTableViewModel.getInstance()
        viewModel.subscribe(observer: self)
        setupTopBarItems()
        self.tableView.register(KontaktCellTableViewCell.self, forCellReuseIdentifier: "reuseCellUser")
        userTableView.delegate = self
        userTableView.dataSource = self
        userTableView.backgroundColor = UIColor.white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        MessageTableViewModel.getInstance().notifyChanges()
    }
    
    func showLoadingDialog(){
        self.present(DialogFactory.instance.createLoadingAnimationDialog(), animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getFriendsList().count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseCellUser", for: indexPath) as! KontaktCellTableViewCell
        cell.nameLabel?.text = viewModel.getFriendsList()[indexPath.row].user?.name
        cell.emailLabel?.text = viewModel.getFriendsList()[indexPath.row].content
        setProfilImage(cell: cell, url: viewModel.getFriendsList()[indexPath.row].user?.profileImageURL)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ChatToUser(userToChat: viewModel.getFriendsList()[indexPath.row]).execute()
        self.performSegue(withIdentifier: "fromMessagetoChat", sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect.zero)
    }
    
    @IBAction func onNewKontaktClick(_ sender: Any) {
        self.performSegue(withIdentifier: "toNewFriendlistController", sender: nil)
    }
    
    
    private func setProfilImage(cell: KontaktCellTableViewCell, url: String?){
        if cell.profilImageView == nil {
            return
        }
        if let linkToImage = url {
            let profilUrl = URL(string: linkToImage)
            cell.profilImageView.sd_setImage(with: profilUrl)
        }
    }
}

extension MessageTableViewController{
    func setupTopBarItems(){
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(onLogoutButtonClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(onSearchKontaktClick))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.systemBlue
        navigationItem.rightBarButtonItem?.tintColor = UIColor.systemBlue
        
        if(viewModel.currentProfilUser != nil) {
            setupNavigtionBarWihtUser(user: viewModel.currentProfilUser!)
        }
    }
    
    func setupNavigtionBarWihtUser(user: User){
        let cqRect = CGRect(x: 0, y: 0, width: 100, height: 40)
        let titleView = NavigationBarTitleWithUserView(cgRect: cqRect, user: user)
        navigationItem.titleView = titleView
    }
    
    
   @objc func onSearchKontaktClick(_ sender: Any) {
       performSegue(withIdentifier: "toFriendsListController", sender: self)
   }
    
    
   @objc func onLogoutButtonClick(_ sender: Any) {
       LogoutUser().execute()
       self.dismiss(animated: true, completion: nil)
   }
    
}

extension MessageTableViewController : ObserverController{
    func update() {
        self.userTableView.reloadData()
        if(viewModel.currentProfilUser != nil) {
            setupNavigtionBarWihtUser(user: viewModel.currentProfilUser!)
        }
    }
}

