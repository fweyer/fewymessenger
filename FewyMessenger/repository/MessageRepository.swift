//
//  MessageRepository.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 01.11.21.
//

import Foundation

class MessageRepository {
    
    private static var instance : MessageRepository?
    private let dataBaseService: DatabaseService
    private var currentUserID: String?
    
  
    private init(){
        self.dataBaseService = FireBaseDatabaseService.instance
    }
    
    static func getInstance () -> MessageRepository {
        if(instance == nil){
            instance = MessageRepository()
        }
        return instance!
    }
    
    func setCurrentUserID(uid: String){
        self.currentUserID = uid
    }
    
    
    func sendMessage(message: Message, complition: @escaping (Message) -> Void){
        dataBaseService.insertMessage(message: message, complition: complition)
    }
    
    func loadNewMessage(currentUserID: String, complition: @escaping (Message) -> Void) {
        dataBaseService.onLoadMessage(uid: currentUserID){ message in
            complition(message)
        }
    }
    
    func reset(){
        if(currentUserID != nil){
            dataBaseService.removeAllObserver(uid: currentUserID!)
        }
    }
    
}
