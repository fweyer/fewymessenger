//
//  UserRepository.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 10.10.21.
//

import Foundation
import FirebaseAuth
import UIKit

// Diese Klasse kümmert sich um die Authentifizierung und Datenspeicherung im Backend
class UserRepository {
    
    private static var instance : UserRepository?
    
    let authService: AuthService
    let dataBaseService: DatabaseService
    let storageService: StorageService
    var autoLogUser: FirebaseAuth.User? = nil
    
    private init(){
        self.authService = FireBaseAuthService.instance
        self.dataBaseService = FireBaseDatabaseService.instance
        self.storageService = FireBaseStorageService.instance
    }
    
    static func getInstance () -> UserRepository {
        if(instance == nil){
            instance = UserRepository()
        }
        return instance!
    }
    
    
    func createUser(user : User, profileImage: UIImage?, onSuccess: @escaping () -> Void, onError: @escaping () -> Void) {
        let succesfulAuthRegistration : (_ result: AuthDataResult?,_ user: User) -> Void = { result, user in
            
            if result == nil {
                onError()
                return
            }
            guard let uid = result?.user.uid else {
                onError()
                return
            }
            
            self.insertImage(image: profileImage, uid: uid, user: user) { updatedUser in
                self.insertUserInDataBase(uid,updatedUser)
                onSuccess()
            }
            
        }
        authService.createUser(user: user, onSuccess: succesfulAuthRegistration, onError: onError)
    }
    
    func userLogin(user: User, onSuccess: @escaping () -> Void, onError: @escaping () -> Void) {
        let setAuthUser :  (_ result: AuthDataResult?) -> Void = { result in
            if result != nil {
                self.autoLogUser = result?.user
            }
            onSuccess()
        }
        authService.userLogin(user: user, onSuccess: setAuthUser, onError: onError)
    }
    
    func insertUserInDataBase(_ uid: String,_ user: User){
        dataBaseService.insertUser(uid, user)
    }
    
    func insertImage(image: UIImage?,  uid : String, user: User, complition: @escaping (_ user: User) -> Void) {
        storageService.insertProfileImage(profileImage: image, uid: uid, user: user, complition: complition)
    }
    
    func userLogout(){
        authService.userLogout()
    }
    
    func fetchUser(complition: @escaping (User) -> Void){
        return dataBaseService.fetchUser(complition: complition)
    }
    
    func fetchCurrentUser(complition: @escaping (User) -> Void){
        guard let uid = authService.autoLogin()?.uid else { return }
        dataBaseService.getCurrentUser(uid: uid, complition: complition)
    }
    
}
