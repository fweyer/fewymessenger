//
//  GlobalDataRepository.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 06.11.21.
//

import Foundation

class GlobalDataHolderRepository{
    
    private static var instance : GlobalDataHolderRepository?
    
    var userList :  [User]
    var messageList :  [Message]
    
    
    private init(){
        self.userList = []
        self.messageList = []
    }
    
    static func getInstance () -> GlobalDataHolderRepository {
        if(instance == nil){
            instance = GlobalDataHolderRepository()
        }
        return instance!
    }
    
    func appendUser(user : User) {
        self.userList.append(user)
    }
    
    func setUserList(userList : [User]) {
        self.userList = userList
    }
    
    func getUserList() -> [User]{
        return self.userList
    }
    
    func appendNewMessage(message: Message){
        self.messageList.append(message)
    }
    
    func getMessageList() -> [Message]{
        return self.messageList
    }
    
    func resetData(){
        self.userList = []
        self.messageList = []
    }
    

    
}
