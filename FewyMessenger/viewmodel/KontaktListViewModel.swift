//
//  NewFriendListViewModel.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 31.10.21.
//

import Foundation

class KontaktListViewModel : ObservableViewModel{
    
    private static var instance : KontaktListViewModel?
    
    var kontaktList: [User]
    var currentProfilUser : User?
    var isKontaktListAlreadyLoaded = false

    private override init(){
        self.kontaktList = []
    }
    
    static func getInstance () -> KontaktListViewModel {
        createViewModel()
        return instance!
    }
    
    static func createViewModel() {
        if(instance == nil){
            instance = KontaktListViewModel()
        }
    }
    
    static func reset () {
        instance = nil
    }
}

