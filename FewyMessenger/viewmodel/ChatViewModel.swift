//
//  ChatViewModel.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 01.11.21.
//

import Foundation

class ChatViewModel : ObservableViewModel {
    
    private static var instance : ChatViewModel?
    
    private var messages : [Message]
    var userToChat : User?
    var currentProfilUser : User?
    
    private override init(){
        messages = []
    }
    
    static func getInstance () -> ChatViewModel {
        createViewModel()
        return instance!
    }
    
    static func createViewModel() {
        if(instance == nil){
            instance = ChatViewModel()
        }
    }
    
    static func reset () {
        instance = nil
    }
    
    func getCount() -> Int {
        return messages.count
    }
    
    func getMessage(index : Int) -> Message {
        return messages[index]
    }
    
    func reloadMessageList(){
        if(userToChat != nil && currentProfilUser != nil){
            messages = MessageUtil.findMessages(
                toUID: userToChat!.uid!,
                messages: GlobalDataHolderRepository.getInstance().getMessageList()
            )
        }
    }
    
}
