//
//  MessageTableViewModel.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 31.10.21.
//

import Foundation

class MessageTableViewModel : ObservableViewModel{
    
    private static var instance : MessageTableViewModel?
    var newestMessages: Set<UserMessageInfo>
    var sortedList : [UserMessageInfo]
    var currentProfilUser : User?
    
    
    private override init(){
        self.newestMessages = []
        self.sortedList = []
    }
    
    static func getInstance () -> MessageTableViewModel {
        createViewModel()
        return instance!
    }
    
    static func createViewModel() {
        if(instance == nil){
            instance = MessageTableViewModel()
        }
    }
    
    func getFriendsList() -> [UserMessageInfo]{
        return self.sortedList
    }
    
    func setNewestMessage(message : Message) {
        let userInfo = UserMessageInfo(content: message.content, fromUserID: message.fromUserID, toUserID: message.toUserID, timestamp: message.timeStamp)
        setUserinfo(message: userInfo)
        contentChange(userInfo: userInfo)
        self.newestMessages.insert(userInfo)
        sortList()
    }
  
    private func setUserinfo(message : UserMessageInfo){
        for user in GlobalDataHolderRepository.getInstance().userList {
            if(message.toUserID == user.uid || message.fromUserID == user.uid){
                message.user = user
            }
        }
    }
    
    private func contentChange(userInfo : UserMessageInfo){
        for message in newestMessages {
            if(message.toUserID == userInfo.user?.uid || message.fromUserID == userInfo.user?.uid){
                message.content = userInfo.content
                message.timeStamp = userInfo.timeStamp
                break
            }
        }
    }
    
    private func sortList(){
        let newest = Array(newestMessages)
        let sorted = newest.sorted { infoOne, infoTwo in
           return infoOne.timeStamp > infoTwo.timeStamp
        }
        MessageTableViewModel.getInstance().sortedList = sorted
    }
    
    
    static func reset () {
        instance = nil
    }
}
