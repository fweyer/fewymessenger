//
//  AuthService.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 09.10.21.
//

import Foundation
import Firebase
import UIKit


protocol AuthService {
    
    func createUser(user: User, onSuccess: @escaping (_ result: AuthDataResult?, _ user: User) -> Void, onError: @escaping () -> Void)
    
    func userLogin(user: User, onSuccess: @escaping (_ result: AuthDataResult?) -> Void, onError: @escaping () -> Void)
    
    func autoLogin() -> FirebaseAuth.User?
    
    func userLogout()
    
    
}
