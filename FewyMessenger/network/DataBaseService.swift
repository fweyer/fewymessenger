//
//  DataBaseServise.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 10.10.21.
//

import Foundation
import Firebase

protocol DatabaseService {
    
    func insertUser(_ uid : String,_ user: User)
    
    func fetchUser(complition: @escaping (User) -> Void)
    
    func getCurrentUser(uid: String, complition: @escaping (User) -> Void)
    
    func insertMessage(message: Message, complition: @escaping (Message) -> Void)
    
    func onLoadMessage (uid: String, complition: @escaping (Message) -> Void)
    
    func removeAllObserver(uid: String)
}
