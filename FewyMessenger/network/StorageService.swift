//
//  StorageService.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 15.10.21.
//

import Foundation
import UIKit

protocol StorageService {
    
    func insertProfileImage(profileImage: UIImage?, uid: String, user: User, complition: @escaping (User) -> Void) 
    
}
