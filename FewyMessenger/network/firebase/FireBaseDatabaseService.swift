//
//  FireBaseDatabaseService.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 10.10.21.
//

import Foundation
import Firebase

class FireBaseDatabaseService : DatabaseService {
    
    static let instance : FireBaseDatabaseService = FireBaseDatabaseService()
    
    private let url = "https://fewymessenger-default-rtdb.europe-west1.firebasedatabase.app"
    private var userRef : DatabaseReference
    private var messageRef: DatabaseReference
    private var userMessageRef: DatabaseReference
    
    private init(){
        userRef = Database.database(url: url).reference().child("users")
        messageRef = Database.database(url: url).reference().child("message")
        userMessageRef = Database.database(url: url).reference().child("user-messages")
    }
    
    func insertUser(_ uid: String,_ user: User) {
        let ref = Database.database(url: url).reference().child("users").child(uid)
        ref.setValue([
            "uid" : uid,
            "name" : user.name,
            "email" : user.email,
            "profileImageURL" : user.profileImageURL ?? "Kein Bild vorhanden"
        ])
    }
    
    func fetchUser(complition: @escaping (User) -> Void) {
        userRef.observe(.childAdded) { data in
            if let dic = data.value as? [String : Any] {
                let user = User(dic: dic)
                print(user)
                complition(user)
            }
        }
    }
    
    func getCurrentUser(uid: String, complition: @escaping (User) -> Void) {
       userRef.child(uid).observeSingleEvent(of: .value) { data in
            if let dic = data.value as? [String : Any] {
                let user = User(dic: dic)
                complition(user)
            }
        }
    }
    
    func insertMessage(message: Message, complition: @escaping (Message) -> Void) {
        let childRef = messageRef.childByAutoId()
        let dic : [String : Any] = [
            "content" : message.content,
            "fromUserID" : message.fromUserID,
            "toUserID" : message.toUserID,
            "timestamp" : message.timeStamp
        ]
        
        childRef.updateChildValues(dic) { error, data in
            if error != nil {
                print(error.debugDescription)
                return
            }
            let senderRef = self.userMessageRef.child(message.fromUserID)
            let empfaengerRef = self.userMessageRef.child(message.toUserID)
            let messageID = childRef.key
            
            let m = [messageID : 1]

            senderRef.updateChildValues(m)
            empfaengerRef.updateChildValues(m)
            
            complition(message)
        }
    }
    
    func onLoadMessage (uid: String, complition: @escaping (Message) -> Void){
        let ref = userMessageRef.child(uid)
        ref.observe(.childAdded) { data in
            let messageID = data.key
            self.messageRef.child(messageID).observeSingleEvent(of: .value) { data in
                    if let dic = data.value as? [String : Any] {
                    let message = Message(dic: dic)
                        print(message.timeStamp)
                    complition(message)
                }
            }
        }
    }
    
    
    func removeAllObserver(uid: String){
        userRef.child(uid).removeAllObservers()
        messageRef.child(uid).removeAllObservers()
        userMessageRef.child(uid).removeAllObservers()
    }
    
    
}
