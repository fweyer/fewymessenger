//
//  FireBaseStorageService.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 15.10.21.
//

import Foundation
import UIKit
import FirebaseStorage

class FireBaseStorageService : StorageService {
    
    static let instance : StorageService = FireBaseStorageService()
    
    private init(){}
    
 
    func insertProfileImage(profileImage: UIImage?, uid: String, user: User, complition: @escaping (User) -> Void) {
        let storageRef = Storage.storage().reference().child("profile_image").child(uid)
        
        guard let image = profileImage else {
            print("Kein Profilfoto übergeben")
            complition(user)
            return
        }
        
        guard let uploadedImage = image.jpegData(compressionQuality: 0.1) else{
            print("Komprimierung des Fotos ist fehlgeschlagen")
            complition(user)
            return
        }
        
        storageRef.putData(uploadedImage, metadata: nil) { metadata, error in
            if error != nil {
                print(error!)
                complition(user)
                return
            }
            
            storageRef.downloadURL { url, error in
                if error != nil {
                    print(error!)
                    return
                }
                
                user.profileImageURL = url?.absoluteString
                complition(user)
            }
        
        }
        
    }
        
}
