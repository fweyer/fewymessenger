//
//  FireBaseAuthService.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 08.10.21.
//

import Foundation
import FirebaseAuth
import UIKit

class FireBaseAuthService : AuthService {

    
    static let instance: AuthService = FireBaseAuthService()
   
    private init (){}
    
    func createUser(user: User, onSuccess: @escaping (_ result: AuthDataResult?,_ user: User) -> Void, onError: @escaping () -> Void) {
        Auth.auth().createUser(withEmail: user.email, password: user.password) { result, error in
            if error != nil {
                print(error!)
                onError()
            }else{
                onSuccess(result, user)
            }
        }
    }
    
    func userLogin(user: User, onSuccess: @escaping (_ result: AuthDataResult?) -> Void, onError: @escaping () -> Void) {
        Auth.auth().signIn(withEmail: user.email, password: user.password) { result, error in
            if error != nil {
                print(error!)
                onError()
            }else{
                onSuccess(result)
            }
        }
    }
    
    func userLogout() {
        do {
            try Auth.auth().signOut()
        } catch  {
            print(error)
        }
    }
    
    func autoLogin() -> FirebaseAuth.User? {
        return Auth.auth().currentUser
    }
    
    
    
}
