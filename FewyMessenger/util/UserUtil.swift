//
//  UserUtil.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 17.10.21.
//

import Foundation

class UserUtil {
    
    static func removeUserFromList(_ email: String, listUsers : [User]) -> [User]{
        var mutableListUsers = listUsers
        
        for (index, user) in listUsers.enumerated() {
            if user.email == email{
                mutableListUsers.remove(at: index)
            }
        }
        return mutableListUsers
    }
    
    static func createUserListFromDic(dic : [String : Any]) -> [User] {
        var userList : [User] = []
        for (key, _) in dic {
            let user = User(dic: dic[key] as! [String : Any])
            userList.append(user)
        }
        return userList
    }
    
}
