//
//  MessageUtil.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 05.11.21.
//

import Foundation

class MessageUtil {
    
    static func createMessageListFromDic(dic : [String : Any]) -> [Message] {
        var messageList : [Message] = []
        for (key, _) in dic {
            let message = Message(dic: dic[key] as! [String : Any])
            messageList.append(message)
        }
        return messageList
    }
    
    static func findMessages(toUID: String, messages : [Message]) -> [Message]{
        var newList : [Message] = []
        for message in messages {
            if(toUID == message.toUserID || toUID == message.fromUserID){
                newList.append(message)
            }
            
        }
        return newList;
    }
    
    
}
