//
//  Validation.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 10.10.21.
//

import Foundation

class Validation {
    
    static func createUserValidated (name: String?, email: String?, password: String?) -> User? {
       guard  name != nil else { return nil }
       guard  email != nil else { return nil }
       guard password != nil else { return nil }
       
       if name!.isEmpty || email!.isEmpty || password!.isEmpty { return nil }
        
       return User(name: name!, email: email!, password: password!)
   }
    
    static func createUserValidated (email: String?, password: String?) -> User? {
       guard  email != nil else { return nil }
       guard password != nil else { return nil }
       
       if email!.isEmpty || password!.isEmpty { return nil }
        
       return User(email: email!, password: password!)
   }
    
    static func validateMessage(content : String?, fromUserID: String?, toUserID : String? ) -> Message? {
        guard  content != nil else { return nil }
        guard  fromUserID != nil else { return nil }
        guard  toUserID != nil else { return nil }
        
        if content!.isEmpty || fromUserID!.isEmpty || toUserID!.isEmpty  { return nil }
        
        return Message(content: content!, fromUserID: fromUserID!, toUserID: toUserID!)
    }
    
    
}
