//
//  LogoutUser.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 01.11.21.
//

import Foundation

class LogoutUser : UseCase{
    func execute() {
        UserRepository.getInstance().userLogout()
        MessageRepository.getInstance().reset()
        ChatViewModel.reset()
        MessageTableViewModel.reset()
        KontaktListViewModel.reset()
        GlobalDataHolderRepository.getInstance().resetData()
    }
    
    
}
