//
//  LoginUser.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 28.10.21.
//

import Foundation
import UIKit

class LoginUseCase : UseCase {
    
    let email: String?
    let password: String?
    
    init(email: String?, password: String?) {
        self.email = email
        self.password = password
    }
   
    func execute() {
        let loginController = LoginsViewController.currentController!
        let user = Validation.createUserValidated(email: email, password: password)
        if user == nil {
            loginController.showErrorDialogPasswordOrEmailWrong()
        }else {
            loginController.showLoadingDialog()
            UserRepository.getInstance().userLogin(user: user!)
            {
                AutoLogin().execute()

            } onError: {
                loginController.showErrorDialogPasswordOrEmailWrong()
            }
        }
    }
}
