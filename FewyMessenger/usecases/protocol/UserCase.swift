//
//  UserCase.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 28.10.21.
//

import Foundation
import UIKit

protocol UseCase {
    
    func execute ()
    
}
