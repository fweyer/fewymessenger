//
//  ChatToUser.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 01.11.21.
//

import Foundation

class ChatToUser : UseCase{
    
    var userToChat : User?
    var userToChatNewMessage : UserMessageInfo?
    
    init(userToChat: User){
        self.userToChat = userToChat
    }
    
    init(userToChat: UserMessageInfo){
        self.userToChatNewMessage = userToChat
        
    }
    
    
    func execute() {
        let chatViewModel = ChatViewModel.getInstance()
        
        if(userToChat != nil){
            chatViewModel.userToChat = userToChat
        }else{
            chatViewModel.userToChat = userToChatNewMessage?.user
        }
    }
    
    
}
