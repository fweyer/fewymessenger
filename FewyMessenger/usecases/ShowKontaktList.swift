//
//  ShowKontaktList.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 29.10.21.
//

import Foundation
import UIKit

class ShowKontaktList: UseCase {
    
    func execute() {
        let viewModel = KontaktListViewModel.getInstance()
            viewModel.kontaktList = GlobalDataHolderRepository.getInstance().getUserList()
            viewModel.notifyChanges()
    }
}
