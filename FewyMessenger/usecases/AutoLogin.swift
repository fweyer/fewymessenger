//
//  AutoLogin.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 29.10.21.
//

import Foundation
import UIKit

class AutoLogin : UseCase{
    
    func execute() {
        let loginController = LoginsViewController.currentController!
        UserRepository.getInstance().fetchCurrentUser { user in
            self.createViewModels(user: user)
            self.observeUsers(currentUserId: user.uid)
            self.observeMessages(currentUserId: user.uid)
            loginController.navigateToMessageTableView()
        }
    }
    
    func createViewModels(user: User){
        ChatViewModel.createViewModel()
        ChatViewModel.getInstance().currentProfilUser = user
        
        MessageTableViewModel.createViewModel()
        MessageTableViewModel.getInstance().currentProfilUser = user
        
        KontaktListViewModel.createViewModel()
        KontaktListViewModel.getInstance().currentProfilUser = user

    }
    
    func observeMessages(currentUserId: String?){
        if currentUserId == nil {return}
        MessageRepository.getInstance().setCurrentUserID(uid: currentUserId!)
        MessageRepository.getInstance().loadNewMessage(currentUserID: currentUserId!) { message in
            MessageTableViewModel.getInstance().setNewestMessage(message: message)
            GlobalDataHolderRepository.getInstance().appendNewMessage(message: message)
            ChatViewModel.getInstance().reloadMessageList()
            ChatViewModel.getInstance().notifyChanges()
            
        }
    }
    
    func observeUsers(currentUserId: String?){
        UserRepository.getInstance().fetchUser() { user in
            if(currentUserId != user.uid){
                GlobalDataHolderRepository.getInstance().appendUser(user: user)
            }
        }
    }
    
    
    
}
