//
//  SendMessage.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 01.11.21.
//

import Foundation

class SendMessage : UseCase{
   
    let content : String?
    
    init(content: String?) {
        self.content = content
    }
    
    func execute() {
        let fromUserID = MessageTableViewModel.getInstance().currentProfilUser?.uid
        let toUserID =   ChatViewModel.getInstance().userToChat?.uid
        let message = Validation.validateMessage(content: content, fromUserID: fromUserID, toUserID: toUserID)
        if message == nil {return}
        MessageRepository.getInstance().sendMessage(message: message!) { message in
            ChatViewModel.getInstance().notifyChanges()
            MessageTableViewModel.getInstance().notifyChanges()
        }
    }
    
    
}
