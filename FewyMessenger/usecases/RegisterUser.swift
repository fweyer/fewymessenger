//
//  RegisterUser.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 29.10.21.
//

import Foundation
import UIKit

class RegisterUser : UseCase  {
    
    let name: String?
    let email: String?
    let password: String?
    
    init(name: String?, email: String?, password: String?){
        self.name = name
        self.email = email
        self.password = password
    }
    
    
    func execute() {
       let registerController = RegisterViewController.currentController!
       let user = Validation.createUserValidated(name: name, email: email, password: password)
        
        if user == nil {
            registerController.showDialogInputDataUnCompled()
        }else{
            performRegistrierung(user: user!, registerController: registerController)
        }
    }
    
    func performRegistrierung(user: User, registerController: RegisterViewController){
        registerController.showLoadingDialog()
        let image = registerController.uiImageView.image
        UserRepository.getInstance().createUser(user: user, profileImage: image)
        {
            //beendet den loading dialog und zeigt einen folgenden dialog an
            registerController.showDialogRegisterSucces()
          
        } onError: {
            //beendet den loading dialog und zeigt einen folgenden dialog an
            registerController.showDialogOnRegisterError()

        }
    }
    
    
}
