//
//  User.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 10.10.21.
//

import Foundation

class User : Hashable, Equatable{
    
    var uid: String?
    let name: String?
    var email: String
    let password: String
    var profileImageURL: String?
    
    
    init(email: String, password: String) {
        self.uid = nil
        self.email = email
        self.password = password
        self.name = nil
        self.profileImageURL = nil
    }
    
    init(uid: String, name: String, email: String, profileImageURL: String) {
        self.uid = uid
        self.email = email
        self.password = ""
        self.name = name
        self.profileImageURL = profileImageURL
    }
    
    
    init(name: String, email: String, password: String) {
        self.uid = nil
        self.name = name
        self.email = email
        self.password = password
        self.profileImageURL = nil
    }
    
    init(dic : [String : Any]){
        self.uid = dic["uid"] as? String
        self.name = dic["name"] as? String
        self.email = dic["email"] as! String
        self.profileImageURL = dic["profileImageURL"] as? String
        self.password = ""
    }
    
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.uid == rhs.uid
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(uid)
    }
    
    
}


