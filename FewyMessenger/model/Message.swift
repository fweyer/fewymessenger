//
//  Message.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 01.11.21.
//

import Foundation

class Message {
    
    var content : String
    let fromUserID : String
    let toUserID : String
    var timeStamp: Int
    
    init(content: String, fromUserID: String, toUserID : String) {
        self.content = content
        self.fromUserID = fromUserID
        self.toUserID = toUserID
        self.timeStamp = Int(Date().timeIntervalSince1970)
    }
    
    init(dic: [String : Any]){
        self.content = dic["content"] as! String
        self.fromUserID = dic["fromUserID"] as! String
        self.toUserID = dic["toUserID"] as! String
        self.timeStamp = dic["timestamp"] as! Int
    }
    

    
}
