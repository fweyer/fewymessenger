//
//  UserMessageInfo.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 07.11.21.
//

import Foundation

class UserMessageInfo : Message, Hashable, Equatable {
    
    var user: User?
    
    init(content: String, fromUserID: String, toUserID: String, timestamp : Int) {
        super.init(content: content, fromUserID: fromUserID, toUserID: toUserID)
        super.timeStamp = timestamp
        
    }
    
    static func == (lhs: UserMessageInfo, rhs: UserMessageInfo) -> Bool {
        return lhs.user?.uid == rhs.user?.uid
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(user?.uid)
    }
    
    
}
