//
//  ChatBubbels.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 22.11.21.
//  Copyright © 2021 fewycoding. All rights reserved.
//

import Foundation
import UIKit

class ChatBubbleCell : UITableViewCell {
    
    static let greenColor = UIColor(red: 107/255, green: 142 / 255, blue: 35 / 255, alpha: 0.8)
    
    let textView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.backgroundColor = UIColor.clear
        textView.textColor = UIColor.white
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        
        return textView
    }()
    
    let bubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = greenColor
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 20
        view.layer.borderWidth = 5
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.masksToBounds = true
        return view
    }()
    
    var bubbleViewShowRight : NSLayoutConstraint?
    var bubbleViewShowLeft : NSLayoutConstraint?
    var bubbleViewWidth : NSLayoutConstraint?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        addSubview(bubbleView)
        addSubview(textView)
        
        textView.leftAnchor.constraint(equalTo: bubbleView.leftAnchor,constant: 8).isActive = true
        textView.rightAnchor.constraint(equalTo: bubbleView.rightAnchor).isActive = true
        textView.topAnchor.constraint(equalTo: bubbleView.topAnchor, constant: 5).isActive = true
        textView.bottomAnchor.constraint(equalTo: bubbleView.bottomAnchor).isActive = true
    
        bubbleViewShowRight = bubbleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8)
        bubbleViewShowLeft = bubbleView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8)
        bubbleViewWidth = bubbleView.widthAnchor.constraint(equalToConstant: 200)
        bubbleViewWidth?.isActive = true
        
        bubbleView.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        bubbleView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func setupFrameForText(text: String)-> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string : text).boundingRect(with: size, options: options, attributes:
                    [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)], context: nil)
    }
    

    
    
}
