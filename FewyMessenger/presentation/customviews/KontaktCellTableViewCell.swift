//
//  KontaktCellTableViewCell.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 29.10.21.
//

import UIKit

class KontaktCellTableViewCell: UITableViewCell {

    @IBOutlet weak var profilImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profilImageView.layer.masksToBounds = false
        profilImageView.layer.borderWidth = 0
        profilImageView.layer.cornerRadius = profilImageView.frame.size.height / 2
        profilImageView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    
    
}
