//
//  DialogFactory.swift
//  FewyMessenger
//
//  Created by Felix Weyer on 09.10.21.
//

import Foundation
import UIKit

class DialogFactory {
    
    static let instance = DialogFactory()
    
    private init(){}
    
    func createLoadingAnimationDialog() -> UIAlertController {
        let loadingAnimationDialog = UIAlertController(title: "", message: nil, preferredStyle: .alert)
        let indicator = UIActivityIndicatorView(frame: loadingAnimationDialog.view.bounds)
        indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        indicator.isUserInteractionEnabled = false
        loadingAnimationDialog.view.addSubview(indicator)
        indicator.startAnimating()
        return loadingAnimationDialog
    }
    
    func createStandardDialog(title: String, message: String) -> UIAlertController {
        let dialog = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let _action = UIAlertAction(title: "Ok", style: .default) { action in }
        dialog.addAction(_action)
        return dialog
    }
    
}
